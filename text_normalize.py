import re
import string
import sys
import unicodedata
from bs4 import BeautifulSoup

accented_chars_vietnamese = [
    'á', 'à', 'ả', 'ã', 'ạ', 'â', 'ấ', 'ầ', 'ẩ', 'ẫ', 'ậ', 'ă', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ',
    'ó', 'ò', 'ỏ', 'õ', 'ọ', 'ô', 'ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'ơ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ',
    'é', 'è', 'ẻ', 'ẽ', 'ẹ', 'ê', 'ế', 'ề', 'ể', 'ễ', 'ệ',
    'ú', 'ù', 'ủ', 'ũ', 'ụ', 'ư', 'ứ', 'ừ', 'ử', 'ữ', 'ự',
    'í', 'ì', 'ỉ', 'ĩ', 'ị',
    'ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ',
    'đ',
]
accented_chars_vietnamese.extend([c.upper() for c in accented_chars_vietnamese])
alphabet = '\x00 ' + string.ascii_letters + string.digits + ''.join(accented_chars_vietnamese) + string.punctuation


def strip_tags(html):
    soup = BeautifulSoup(html)
    return soup.get_text()


def Text(text):
    """ provide a wrapper for python string
    map byte to str (python 3)
    map str to unicode (python 2)
    all string in utf-8 encoding
    normalize string to nFC
    """
    if not is_unicode(text):
        text = text.decode("utf-8")
    text = unicodedata.normalize("NFC", text)
    return text


def is_unicode(text):
    if sys.version_info >= (3, 0):
        unicode_type = str
    else:
        unicode_type = unicode
    return type(text) == unicode_type


def create_patterns():
    word = "\w+"
    non_word = "[^\w\s]"
    abbreviations = [
        "[A-ZĐ]+\.",
        "Tp\.",
        "Mr\.", "Mrs\.", "Ms\.",
        "Dr\.", "ThS\."
    ]
    email = r"[-_.0-9A-Za-z]{1,64}@[-_0-9A-Za-z]{1,255}[-_.0-9A-Za-z]{1,255}"
    web = r"https?://[-_.?&~;+=/#0-9A-Za-z]{1,2076}"
    datetime = [
        "\d{1,2}\/\d{1,2}(\/\d+)?",
        "\d{1,2}-\d{1,2}(-\d+)?",
    ]

    patterns = []
    patterns.extend(abbreviations)
    patterns.extend([web, email])
    patterns.extend(datetime)
    patterns.extend([non_word, word])
    return "(" + "|".join(patterns) + ")"


__PATTERNS__ = create_patterns()


def sylabelize(text):
    tokens = re.findall(__PATTERNS__, text, re.UNICODE)
    words = ["%s" % token[0].strip(' ') for token in tokens]
    text = " ".join(words)

    split_text = [i for i in text if i in alphabet]
    text = "".join(x for x in split_text)

    text = " ".join(i for i in text.split())
    return text


def clean_text(raw):
    text = Text(raw)
    text = sylabelize(text)
    text = strip_tags(text)
    return text
