import ast
import json
import re
from os.path import join, dirname

from sklearn.model_selection import train_test_split

from text_normalize import clean_text


def write(data, name):
    filename = join(dirname(__file__), "asset", "raw", name)
    f = open(filename, "a")
    for item in data:
        line = json.dumps(item, ensure_ascii=False)
        f.write(line + '\n')


def convert_to_corpus(data_path):
    posts = []
    with open(data_path) as f:
        content = f.read().lower()
        records = json.loads(content)["records"]
    for item in records:
        row = {}
        row["title"] = clean_text(item["title"])
        row["abstract"] = clean_text(item["description"])
        try:
            keywords = ast.literal_eval(item["keyword"])
            row["keyword"] = ";".join(i for i in keywords)
        except:
            items = [i for i in item["keyword"].split(",")]
            matched = [re.findall("\"(.*?)\"", i) for i in items]
            keywords = [i.strip() for match in matched for i in match if len(i) > 0]
            row["keyword"] = row["keyword"] = ";".join(i for i in keywords)
        posts.append(row)
    train, dev = train_test_split(posts, train_size=0.9)
    train, test = train_test_split(train, train_size=0.8)
    write(train, "kp20k_training.json")
    write(dev, "kp20k_validation.json")
    write(test, "kp20k_testing.json")


if __name__ == '__main__':
    data_file = join(dirname(__file__), "asset", "raw", "news_car_ex.json")
    convert_to_corpus(data_file)
