import argparse
import os

import torch

import config
import pykp.io

parser = argparse.ArgumentParser(
    description='preprocess.py',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-source_dataset_dir', required=True,
                    help="The path to the source data (raw json).")
parser.add_argument('-output_path_prefix', default='data',
                    help="Output file for the prepared data")

config.preprocess_opts(parser)
opt = parser.parse_args()

# input path of each json file
opt.source_train_file = os.path.join(opt.source_dataset_dir, 'kp20k_training.json')
opt.source_valid_file = os.path.join(opt.source_dataset_dir, 'kp20k_validation.json')
opt.source_test_file = os.path.join(opt.source_dataset_dir, 'kp20k_testing.json')

# output path for exporting the processed dataset
opt.output_path = os.path.join(opt.output_path_prefix)

if not os.path.exists(opt.output_path):
    os.makedirs(opt.output_path)


def main():
    src_fields = ['title', 'abstract']
    trg_fields = ['keyword']
    valid_check = True

    print("Loading training/validation/test data...")
    tokenized_train_pairs = pykp.io.load_src_trgs_pairs(source_json_path=opt.source_train_file,
                                                        dataset_name="kp20k",
                                                        src_fields=src_fields,
                                                        trg_fields=trg_fields,
                                                        opt=opt,
                                                        valid_check=valid_check)

    tokenized_valid_pairs = pykp.io.load_src_trgs_pairs(source_json_path=opt.source_valid_file,
                                                        dataset_name="kp20k",
                                                        src_fields=src_fields,
                                                        trg_fields=trg_fields,
                                                        opt=opt,
                                                        valid_check=valid_check)

    tokenized_test_pairs = pykp.io.load_src_trgs_pairs(source_json_path=opt.source_test_file,
                                                       dataset_name="kp20k",
                                                       src_fields=src_fields,
                                                       trg_fields=trg_fields,
                                                       opt=opt,
                                                       valid_check=valid_check)

    print("Building Vocab...")
    word2id, id2word, vocab = pykp.io.build_vocab(tokenized_train_pairs, opt)
    print('Vocab size = %d' % len(vocab))
    if opt.vocab_size > len(vocab):
        opt.vocab_size = len(vocab)
        print('Reset vocab size to %d' % opt.vocab_size)

    print("Dumping dict to disk")
    opt.vocab_path = os.path.join(opt.output_path, "kp20k" + '.vocab.pt')
    torch.save([word2id, id2word, vocab], open(opt.vocab_path, 'wb'))

    pykp.io.process_and_export_dataset(tokenized_train_pairs,
                                       word2id, id2word,
                                       opt,
                                       opt.output_path,
                                       dataset_name="kp20k",
                                       data_type='train',
                                       include_original=True)

    pykp.io.process_and_export_dataset(tokenized_valid_pairs,
                                       word2id, id2word,
                                       opt,
                                       opt.output_path,
                                       dataset_name="kp20k",
                                       data_type='valid',
                                       include_original=True)

    pykp.io.process_and_export_dataset(tokenized_test_pairs,
                                       word2id, id2word,
                                       opt,
                                       opt.output_path,
                                       dataset_name="kp20k",
                                       data_type='test',
                                       include_original=True)


if __name__ == "__main__":
    main()
