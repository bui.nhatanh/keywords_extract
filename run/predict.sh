#!/usr/bin/env bash
directory_exp='./keyphrase/exp/'
directory_data='./keyphrase/asset/'

srun python -m predict  -vocab ${directory_data}/corpus/kp20k.vocab.pt -save_data ${directory_data}/corpus/kp20k -test_data ${directory_data}/raw/kp20k_testing.json -model_path  ${directory_exp}/kp20k.bi-directional.no-loss-mask.20171117-214914/kp20k.epoch=1.batch=8000.total_batch=8000.model -bidirectional
